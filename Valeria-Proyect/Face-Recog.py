# https://clemenssiebler.com/face-recognition-with-azure-cognitive-services-face-api/
# pip install cognitive_face
import cognitive_face as CF

# Setting cognitive services
SUBSCRIPTION_KEY = 'da6dd01edd3d452f83be14067ec192cf'
BASE_URL = 'https://westus2.api.cognitive.microsoft.com/face/v1.0/'
PERSON_GROUP_ID = '1bf73ad9-1aac-4cb4-b679-2efea394ee0c'
CF.BaseUrl.set(BASE_URL)
CF.Key.set(SUBSCRIPTION_KEY)

# take Frame analisis
#response = CF.face.detect('http://181.199.66.129/BodyTracking/Person/VideoFrame/Captura/capture.jpg')
#response = CF.face.detect('C:/Users/Mijail QA/Documents/ToolAutomateCliente/toolautometricaspy/Valeria-Proyect/test.jpg',attributes='age,gender,emotion,hair')
response = CF.face.detect('C:/Users/Mijail/Desktop/toolautometricaspy/Valeria-Proyect/test.jpg',attributes='age,gender,emotion,hair')


# coordenadas de las caras detectadas
print ("///----///", response)
print ("--------******************-------------")
face_ids = [d['faceId'] for d in response]
print ("--------******************-------------")
print ("Faces detectadas")
print (face_ids)
print ("--------******************-------------")
identified_faces = CF.face.identify(face_ids, PERSON_GROUP_ID)
print (identified_faces)

print ("--------******************-------------")
print ("------------**********---------")

