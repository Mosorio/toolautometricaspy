# Truzz Blogg - Youtube link: https://www.youtube.com/watch?v=KcjHfnCteZg
# Speech recognition in Python ::: How to convert Speech to Text
# En windows hay que installar
# pip install pipwin
# pipwin install pyaudio
# Version de Python 3.7.5

import speech_recognition as sr

r = sr.Recognizer()

with sr.Microphone() as source:
    print("Say Something...")
    audio = r.listen(source)

    try:
        # Ingles
        #text = r.recognize_google(audio, language='EN')
        # Español
        text = r.recognize_google(audio, language='ES')
        print("What did you say: {}".format(text))
    except:
        print("I am sorry! I can not understand!")