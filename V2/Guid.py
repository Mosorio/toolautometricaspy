# Python3 code to generate the 
# random id using uuid1() 
  
import uuid 

#////////////////////////
# Ejemplo Uno 
#///////////////////////

# Id generated using uuid4() 
id = uuid.uuid4() 
print (id) 
print ("\n")

#////////////////////////
# Ejemplo Dos 
# Mediante Una Funcion
#///////////////////////

def genereid():
    ids = uuid.uuid4() 
    print (ids)

# llamando funciones 
genereid()
genereid()
genereid()
genereid()
genereid()
genereid()
