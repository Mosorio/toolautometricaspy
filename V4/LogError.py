import json
import os, sys
from io import open
import smtplib
import pymysql.cursors
import datetime 
import time
import uuid 



#///////////////////////////////////////////
# GET-Endpoint-Info
# Funcion para obtener la Informacion de Endpoint, desde el archivo de LayoutTemplate
#//////////////////////////////////////////

def getinfo():
    with open("C:/KioskServicesMedia/Exhibit Media/LayoutTemplate.json") as contenido:
        EndpointInfo = json.load(contenido)

        #encoded
        data_string = json.dumps(EndpointInfo)

        #Decoded
        decoded = json.loads(data_string)

        data_string = json.dumps(EndpointInfo)      
       
        # Asignamos a una variable el resultado,
        CustomerId = str(decoded["EndpointInfo"]["CustomerId"])
        StoreId = str(decoded["EndpointInfo"]["StoreId"])
        EndpointId = str(decoded["EndpointInfo"]["EndpointId"])
        HostName = str(decoded["EndpointInfo"]["EndpointName"])

       
        GetInfoENdpoint  = [CustomerId, StoreId,EndpointId, HostName]
        #print (GetInfoENdpoint)
        return GetInfoENdpoint

#/////////////////////////////////////
#variables de Entorno
# Generamos Un GUID para Identificar la prueba
IdUnico = uuid.uuid4()
# Convertimos el GUID obtenido con "uuid.uuid4", en una cadena para poder guardar en la DB
# la Variable "GuidTest" define el Id de la Prueba Actual, a demas este valor es constante durante toda la prueba.
GuidTest = str(IdUnico)

def GetVersionClient():
    #//////////////////////////////////
	# Verificar la cantidad de archivos que existen dentro del directorio
	# Indicamos la ruta de donde se quieren leer los archivos
    path= "C:/ProgramData/Vsblty/KingSalmon/"
    dirs = os.listdir(path)
    #SearchVersion = "AppVersion"
    for file in dirs:
        ruta = "C:/ProgramData/Vsblty/KingSalmon/"
        archivo = file
        rutacompleta = ruta + archivo
        
        # La aplicaicon lee y guarada en la variable "archivo_texto" toda la informacion 
        archivo_texto=open (rutacompleta, "r")

        # Se lee Linea por linea
        lineas_texto=archivo_texto.readlines()

        #---------------- Inicio de Busqueda
        # Buscar segun el valor del parametro
        # Por cada linea se valida si contiene el Texto que fue eniado como parametro 

        for ClientLine in lineas_texto:
            if "AppVersion" in ClientLine: #busqueda por el parametro "TextLogSearch"
                AppVersion = ClientLine[139:150]
    return AppVersion
AppVersion = GetVersionClient()

#//////////////////////////////////////////
# Connect to the database
# Funcion getinfo()
def EndpointTest():
    getinfodos = getinfo()
    EndpointId = getinfodos[2]
    print (EndpointId)
    connection = pymysql.connect(host='192.168.2.147',
        user='qatest',
        password='Quito.2019',
        db='tooltest',
        charset='utf8mb4',
        cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT `TestNumero` FROM `error_log` WHERE `EndpointId`=%s ORDER BY `TestNumero` DESC LIMIT 1"
            cursor.execute(sql, (EndpointId,))
            result = cursor.fetchone()
            print (result)
            if result == None:
                TestNumero = 1
                print (TestNumero)
            else:
                TestNumero = int(result.get('TestNumero')) + 1
                print (TestNumero)
            #input ()
    finally:
        connection.close()
    return TestNumero
TestNumero = EndpointTest()
#-------------------------- Bloque 1 --------------
# Funcion para realizar busquedas dentro de los archivos Logs 
# Esta funcion recorre todos los archivos Logs busca las Lineas que contengan el valor del parametro "TextLogSearch"
# Recortando

def LogSearch(GuidTest, TextLogSearch, TestNumero):

	#//////////////////////////////////
	# Definimos las Lista
    LogList = []
    ItemLog = 0
    
	#//////////////////////////////////
	# Verificar la cantidad de archivos que existen dentro del directorio
	# Indicamos la ruta de donde se quieren leer los archivos
    path= "C:/ProgramData/Vsblty/KingSalmon/"
    dirs = os.listdir(path)
    #SearchVersion = "AppVersion"
    for file in dirs:
        ruta = "C:/ProgramData/Vsblty/KingSalmon/"
        archivo = file
        rutacompleta = ruta + archivo
        
        # La aplicaicon lee y guarada en la variable "archivo_texto" toda la informacion 
        archivo_texto=open (rutacompleta, "r")

        # Se lee Linea por linea
        lineas_texto=archivo_texto.readlines()

        #---------------- Inicio de Busqueda
        # Buscar segun el valor del parametro
        # Por cada linea se valida si contiene el Texto que fue eniado como parametro 

        for ClientLine in lineas_texto:
            if "AppVersion" in ClientLine: #busqueda por el parametro "TextLogSearch"
                AppVersion = ClientLine[139:150]
        
        for ClientLine in lineas_texto:
                #////////////////////////////////////
                if TextLogSearch in ClientLine: # busqueda por el parametro "TextLogSearch" 
					# por cada Linea que contenga el parametro sacamos
                    LogDateLine=ClientLine[:19] # Fecha Linea Log
                    LogInfo = ClientLine[38:] # Informacion del Log (Informacion de la Linea)
                    ItemLog = ItemLog + 1 # Numero de Item o Log
                    ReadFile  = [ItemLog, LogDateLine, LogInfo, TextLogSearch, archivo, AppVersion]
                    LogList.append(ReadFile)
                    #print (ReadFile)
                    #print (LogList)

                    
                    # ///////////////////////////////
                    # preparar Conexio a La DB
                    #/////////////////////////
                    #print ("Count List:", CountPesronList)
                    # Connect to the database
                    connection = pymysql.connect(host='192.168.2.147',
                        user='qatest',
                        password='Quito.2019',
                        db='tooltest',
                        charset='utf8mb4',
                        cursorclass=pymysql.cursors.DictCursor)

                    try:
                        with connection.cursor() as cursor:
                    # Create a new record
                            
                            # ////////////////////////////
                            # Obtener Informacion de Endpoint
                            # Funcion getinfo()
                            getinfodos = getinfo()
                            CustomerId = getinfodos[0]
                            EndpointId = getinfodos[2]
                            HostName = getinfodos[3]
                            DateTest1 = datetime.datetime.now()
                            
                            
                          
                            #sql = "INSERT INTO `matchprobability` (`GuidTest`, `CustomerId`, `EndpointId`, `Hostname`, `DataTest`, `TestNumero`) VALUES (%s, %s, %s, %s, %s, %s)"
                            #cursor.execute(sql, (GuidTest, CustomerId, EndpointId, HostName, dateTest, TestNumero))
                                                        
                            sql = "INSERT INTO `error_log` (`GuidTest`, `DateTest`, `CustomerId`, `EndpointId`, `HostName`, `AppVersion`, `TestNumero`, `ErrorNumero`,  `ErrorInfo`,  `ErrorFile`, `ErrorTag`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                            cursor.execute(sql,(GuidTest, DateTest1, CustomerId, EndpointId, HostName, AppVersion, TestNumero, ItemLog, LogInfo, archivo, TextLogSearch))
                           
                            #cursor.execute(sql, (ErrorClient, ErrorClient, ErrorClient, ErrorClient, ErrorClient, ErrorClient, ErrorClient))


                    # connection is not autocommit by default. So you must commit to save
                    # your changes.
                            connection.commit()

                    #with connection.cursor() as cursor:
                    # Read a single record
                    #sql = "SELECT `id`, `password` FROM `users` WHERE `email`=%s"
                    #cursor.execute(sql, ('webmaster@python.org',))
                    #result = cursor.fetchone()
                    #print(result)
                    finally:
                        connection.close()

                    
                        
    #return ReadFile
    return LogList

print ("\n")
print ("********************************** [CLOUD Detection] **********************************************")
print ("**********************************************************************")
print ("\n")
print ("********************************** List Errors ****************************************************")
ListErrors = LogSearch (GuidTest, "[Unhandled Exception][FORCED RESTART]", TestNumero)

# total de errores detectadis
StatusTest = "Passed"
TotalErrores = len(ListErrors)
if TotalErrores > 0:
    StatusTest = "Failed"
    AppVersion = ListErrors[-1][-1]
else:
    StatusTest = "Pass"

for ListError in ListErrors:
    print (ListError)
#print ("**********************************************************************")
print ("\n")
print ("********************************** Fin Task **********************************************")
# /////////////////////////////////
# Send Email
getinfodos = getinfo()
CustomerId = getinfodos[0]
EndpointId = getinfodos[2]
HostName = getinfodos[3]
DateTest1 = datetime.datetime.now()


                          
message = "Report Test Automatizado Errores \n" + "-- Test Numero: " + str(TestNumero) + "\n" + "-- Version: " + AppVersion + "\n"+ "-- Status Test: " + StatusTest + "\n" +"-- Hostname: " + HostName + "\n" + "-- EndpoinID: " + EndpointId + "\n" + "-- Errores Detectados: " + str(TotalErrores) + "\n" + str(ListErrors) + "\n"
asunto = "*** Test Numero: " + str(TestNumero) + " *** Guid: " + GuidTest
subject = "*** Hostname: " + HostName + " " + asunto 
message = "Subject: {}\n\n{}".format(subject, message)

server = smtplib.SMTP("smtp.gmail.com", 587)
server.starttls()
server.login("mijail.test4@gmail.com", "58722191")
    
server.sendmail("mijail.test4@gmail.com", "mijail.test7@gmail.com", message)
server.sendmail("mijail.test4@gmail.com", "mijail.test2@gmail.com", message)

server.quit()

print ("Correo enviado correctamente!")


