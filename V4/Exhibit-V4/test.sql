-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2019 at 10:10 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tooltest`
--

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `GuidTest` varchar(100) NOT NULL,
  `EndpointId` varchar(100) NOT NULL,
  `Hostname` varchar(50) NOT NULL,
  `DateTest` varchar(50) NOT NULL,
  `VersionClient` varchar(50) NOT NULL,
  `TestNumero` int(10) NOT NULL,
  `CamaraType` varchar(30) NOT NULL,
  `CollectAnalytics` varchar(10) NOT NULL,
  `Pollingcamera` varchar(10) NOT NULL,
  `DemograhicDataExpiration` varchar(10) NOT NULL,
  `EmailExpiration` varchar(10) NOT NULL,
  `EmailThreshold` varchar(10) NOT NULL,
  `EnableUseVizsafe` varchar(10) NOT NULL,
  `BodyDetection` varchar(10) NOT NULL,
  `Sad` varchar(10) NOT NULL,
  `IdentitySearch` varchar(10) NOT NULL,
  `LiveEndpointData` varchar(10) NOT NULL,
  `ObjectDetection` varchar(10) NOT NULL,
  `FaceDetectionAggressiveness` varchar(10) NOT NULL,
  `OpenVinoFace` varchar(10) NOT NULL,
  `FaceDetectionModel` varchar(10) NOT NULL,
  `PersonDetectionType` varchar(10) NOT NULL,
  `ReocurringVisitor` varchar(10) NOT NULL,
  `TiempoTest` varchar(10) NOT NULL,
  `ModoIdentificacion` varchar(30) NOT NULL,
  `EmailTest` varchar(10) NOT NULL,
  `IdentificacionTest` varchar(10) NOT NULL,
  `SadTest` varchar(10) NOT NULL,
  `RDemograficaTest` varchar(10) NOT NULL,
  `ErroresTest` varchar(10) NOT NULL,
  `Status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
