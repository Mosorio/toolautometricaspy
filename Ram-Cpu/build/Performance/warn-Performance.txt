
This file lists modules PyInstaller was not able to find. This does not
necessarily mean this module is required for running you program. Python and
Python 3rd-party packages include a lot of conditional or optional module. For
example the module 'ntpath' only exists on Windows, whereas the module
'posixpath' only exists on Posix systems.

Types if import:
* top-level: imported at the top-level - look at these first
* conditional: imported within an if-statement
* delayed: imported from within a function
* optional: imported within a try-except-statement

IMPORTANT: Do NOT post this list to the issue-tracker. Use it as a basis for
           yourself tracking down the missing module. Thanks!

missing module named resource - imported by posix (top-level), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level)
missing module named posix - imported by os (conditional, optional), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level)
missing module named _posixsubprocess - imported by subprocess (conditional), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level)
missing module named readline - imported by cmd (delayed, conditional, optional), code (delayed, conditional, optional), pdb (delayed, optional), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level)
excluded module named _frozen_importlib - imported by importlib (optional), importlib.abc (optional), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level)
missing module named _frozen_importlib_external - imported by importlib._bootstrap (delayed), importlib (optional), importlib.abc (optional), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level)
missing module named _winreg - imported by platform (delayed, optional), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level)
missing module named _scproxy - imported by urllib.request (conditional)
missing module named java - imported by platform (delayed), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level)
missing module named 'java.lang' - imported by platform (delayed, optional), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level), xml.sax._exceptions (conditional)
missing module named vms_lib - imported by platform (delayed, conditional, optional), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level)
missing module named termios - imported by tty (top-level), psutil._compat (delayed, optional), getpass (optional), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level)
missing module named grp - imported by shutil (optional), tarfile (optional), pathlib (delayed), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level)
missing module named 'org.python' - imported by pickle (optional), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level), xml.sax (delayed, conditional)
missing module named pwd - imported by posixpath (delayed, conditional), shutil (optional), tarfile (optional), http.server (delayed, optional), webbrowser (delayed), psutil (optional), pathlib (delayed, conditional, optional), getpass (delayed), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level), netrc (delayed, conditional)
missing module named org - imported by copy (optional), C:\Users\Mijail QA\Documents\ToolAutomateCliente\toolautometricaspy\Ram-Cpu\Performance.py (top-level)
missing module named ConfigParser - imported by pymysql.optionfile (conditional)
missing module named 'cryptography.hazmat' - imported by pymysql._auth (optional)
missing module named cryptography - imported by pymysql._auth (optional)
missing module named __builtin__ - imported by PIL.Image (optional), pymysql._compat (conditional)
missing module named _uuid - imported by uuid (optional)
missing module named netbios - imported by uuid (delayed)
missing module named win32wnet - imported by uuid (delayed)
missing module named 'Xlib.XK' - imported by pyautogui._pyautogui_x11 (top-level)
missing module named 'Xlib.ext' - imported by pyautogui._pyautogui_x11 (top-level)
missing module named Xlib - imported by mouseinfo (conditional), pyautogui._pyautogui_x11 (top-level)
missing module named 'Xlib.display' - imported by pyautogui._pyautogui_x11 (top-level)
missing module named AppKit - imported by pyperclip (delayed, conditional, optional), mouseinfo (conditional), pyautogui._pyautogui_osx (top-level)
missing module named Quartz - imported by mouseinfo (conditional, optional), pygetwindow._pygetwindow_macos (top-level), pyautogui._pyautogui_osx (optional)
missing module named Tkinter - imported by pymsgbox (conditional, optional), mouseinfo (conditional, optional), PIL.ImageTk (conditional)
missing module named olefile - imported by PIL.FpxImagePlugin (top-level), PIL.MicImagePlugin (top-level)
missing module named UserDict - imported by PIL.PdfParser (optional)
missing module named 'PySide.QtCore' - imported by PIL.ImageQt (conditional, optional)
missing module named 'PyQt4.QtCore' - imported by PIL.ImageQt (conditional, optional)
missing module named 'PySide2.QtCore' - imported by PIL.ImageQt (conditional, optional)
missing module named PySide2 - imported by PIL.ImageQt (conditional, optional)
missing module named 'PyQt5.QtCore' - imported by PIL.ImageQt (conditional, optional)
missing module named numpy - imported by PIL.ImageFilter (optional), pyscreeze (optional)
missing module named pathlib2 - imported by PIL.Image (optional)
missing module named cffi - imported by PIL.Image (optional), PIL.PyAccess (top-level), PIL.ImageTk (delayed, conditional, optional)
missing module named PIL._imagingagg - imported by PIL (delayed, conditional, optional), PIL.ImageDraw (delayed, conditional, optional)
missing module named PyQt4 - imported by pyperclip (delayed, conditional, optional)
missing module named PyQt5 - imported by pyperclip (delayed, conditional, optional)
missing module named Foundation - imported by pyperclip (delayed, conditional, optional)
missing module named 'PyQt4.QtGui' - imported by pyperclip (delayed, optional)
missing module named 'PyQt5.QtWidgets' - imported by pyperclip (delayed, optional)
missing module named qtpy - imported by pyperclip (delayed, conditional, optional)
missing module named gtk - imported by pyperclip (delayed, conditional, optional)
missing module named cv2 - imported by pyscreeze (optional)
missing module named psutil._psutil_aix - imported by psutil (top-level), psutil._psaix (top-level)
missing module named psutil._psutil_sunos - imported by psutil (top-level), psutil._pssunos (top-level)
missing module named psutil._psutil_bsd - imported by psutil (top-level), psutil._psbsd (top-level)
missing module named psutil._psutil_osx - imported by psutil (top-level), psutil._psosx (top-level)
missing module named _psutil_linux - imported by psutil (conditional)
missing module named psutil._psutil_posix - imported by psutil (top-level), psutil._pslinux (top-level), psutil._psosx (top-level), psutil._psbsd (top-level), psutil._pssunos (top-level), psutil._psaix (top-level)
missing module named fcntl - imported by psutil._compat (delayed, optional)
missing module named _dummy_threading - imported by dummy_threading (optional)
